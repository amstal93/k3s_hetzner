resource "kubectl_manifest" "cluster_autoscaler" {
  depends_on = [
    null_resource.master,
  ]

  for_each  = data.kubectl_file_documents.cluster_autoscaler.manifests
  yaml_body = each.value

  force_new = true
  wait      = true
}

data "external" "hcloud_token" {
  program = ["sh", "-c", <<EOT
echo {\"token\":\"$HCLOUD_TOKEN\"}
EOT
]
}

data "template_file" "master_cloud_config" {
  template  = file("files/master_cloud_config.yml")
  vars      = {
    key               = hcloud_ssh_key.key.public_key,
    k3s_channel       = var.k3s_channel,
    k3s_token         = random_string.k3s_token.result,
    k3s_lb_public_ip  = local.k3s_lb_public_ip,
    k3s_private_ip    = local.master_private_ip,
    count             = count.index,
    hcloud_token      = data.external.hcloud_token.result.token,
    hcloud_network    = hcloud_network.kube.id,
    cluster_cidr      = var.cluster_cidr,
  }

  count               = var.enable_multi_master? 3: 1
}

data "template_file" "node_cloud_config" {
  template  = file("files/node_cloud_config.yml")
  vars      = {
    key             = hcloud_ssh_key.key.public_key,
    k3s_channel     = var.k3s_channel,
    k3s_token       = random_string.k3s_token.result,
    k3s_private_ip  = local.k3s_private_ip,
  }
}

data "template_file" "cluster_autoscaler" {
  template  = file("files/cluster_autoscaler.yml")
  vars      = {
    cloud_init      = base64encode(data.template_file.node_cloud_config.rendered),
    hcloud_network  = hcloud_network.kube.id,
  }
}

data "kubectl_file_documents" "cluster_autoscaler" {
  content = templatefile("files/cluster_autoscaler.yml", {
    cloud_init      = base64encode(data.template_file.node_cloud_config.rendered),
    hcloud_network  = hcloud_network.kube.name,
  })
}

locals {
  k3s_lb_public_ip          = length(hcloud_load_balancer.master) > 0? hcloud_load_balancer.master[0].ipv4 : 0
  master_private_ip         = cidrhost(var.network_subnet, 1)
  load_balancer_private_ip  = cidrhost(var.network_subnet, -2)
  k3s_private_ip            = length(hcloud_load_balancer.master) > 0? cidrhost(var.network_subnet, -2) : cidrhost(var.network_subnet, 1)
}

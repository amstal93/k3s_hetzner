k3s Hetzner
===========

How to build a kubernetes cluster in [Hetzner Cloud](https://www.hetzner.com/cloud).

## Why

Hetzner is a good cloud provider in Europe, this project show us how build a kubernetes cluster in Hetzner Cloud, using [k3s](https://k3s.io/), [cloud-init](https://cloudinit.readthedocs.io/en/latest/) and [Terraform](https://www.terraform.io/).

## Requirements.

- A Hetzner Cloud account.

## Packages used by the build process

- Cloud Init
- Terraform 0.13.x

we get k3s 1.21 version installed.

before execute the configuration, you need to setup your Hetzner Cloud API Key. you must setup this in your Hetzner account following [this  indications](https://docs.hetzner.cloud/#getting-started) and setting up the environment var `$HCLOUD_TOKEN`.

the Kubeconfig file will be setuop in `/tmp/kube_config.yaml` and you can do:

```
export KUBECONFIG=/tmp/kube_config.yaml
```

and after this, execute locally `kubectl` and `helm`.

Execution

```
export HCLOUD_TOKEN="HETZNER_CLOUD_API_TOKEN"
export KUBECONFIG=/tmp/kube_config.yaml
terraform plan -out run.plan
terraform apply "run.plan"
```

the first 4 minutes are for the master insances build and load balancer if is needed, after this, the nodes are build too. You can check executing this commands:

```
kubectl get nodes
kubectl get pods -Aowide
```

resource "hcloud_server" "node" {
  depends_on = [
    null_resource.master,
  ]

  name                = format(var.node_name_format, count.index + 1)
  server_type         = var.node_type
  image               = var.image
  location            = var.location
  placement_group_id  = hcloud_placement_group.kube_resource_group.id
  user_data           = data.template_file.node_cloud_config.rendered

  count               = var.node_count
}

resource "hcloud_server_network" "node" {
  server_id   = hcloud_server.node[count.index].id
  subnet_id   = hcloud_network_subnet.kube_subnet.id
  count       = var.node_count
}

resource "null_resource" "node" {
  depends_on  = [
    hcloud_server.node,
    hcloud_server_network.node
  ]

  triggers = {
    node_ids = hcloud_server.node[count.index].id
  }

  count       = var.node_count

  provisioner "local-exec" {
    command = "until ssh -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${var.cert_private} ubuntu@${hcloud_server.node[count.index].ipv4_address} 'while [ ! -f /var/lib/cloud/instance/boot-finished  ]; do sleep 1; done'; do sleep 1; done"
  }
}

resource "hcloud_firewall" "master_firewall" {
  name = var.master_firewall_name

  rule {
    description = "allow ICMP"
    direction   = "in"
    protocol    = "icmp"
    source_ips = [
      "0.0.0.0/0",
    ]
  }

  rule {
    description = "allow SSH from any"
    direction   = "in"
    protocol    = "tcp"
    port        = "22"
    source_ips = [
      "0.0.0.0/0",
    ]
  }

  rule {
    description = "allow 6443 to master"
    direction   = "in"
    protocol    = "tcp"
    port        = "6443"
    source_ips  = [
      "0.0.0.0/0",
    ]
  }

}

resource "hcloud_firewall" "worker_firewall" {
  name = var.worker_firewall_name
  rule {
    description = "allow ICMP"
    direction   = "in"
    protocol    = "icmp"
    source_ips = [
      "0.0.0.0/0",
    ]
  }

  rule {
    description = "allow SSH from any"
    direction   = "in"
    protocol    = "tcp"
    port        = "22"
    source_ips = [
      "0.0.0.0/0",
    ]
  }

}
